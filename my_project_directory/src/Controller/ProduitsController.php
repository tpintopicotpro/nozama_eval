<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Form\ProduitType;
use App\Entity\Produits;
use App\Entity\Tags;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\Form\Extension\Core\Type\{ChoiceType,TextType,ButtonType,EmailType,HiddenType,PasswordType,TextareaType,SubmitType,NumberType,DateType,MoneyType,BirthdayType};
use App\Repository\ProduitsRepository;
use App\Repository\TagsRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Doctrine\ORM\PersistentCollection;
use Symfony\Component\Form\ChoiceList\ChoiceList;

class ProduitsController extends AbstractController
{
    #[Route('/', name: 'app_home')]
    public function accueil(PaginatorInterface $paginator,ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {   
        $tags = $doctrine->getRepository(Tags::class)->findAll();
        
        $titre_options = array();
        foreach($tags as $elm)
    {
    $titre = $elm->getTitre();
    array_push($titre_options ,$titre);
    }
        $form = $this->createFormBuilder()
        ->setAction($this->generateUrl('handleSearch'))
        ->add('query', TextType::class, [
            'label' => false,
            'attr' => [
                'class' => 'form-control',
                'placeholder' => 'Entrez un mot-clé'
            ]
        ])
        // ->add('Tags', ChoiceType::class, [
        //     'attr' => [
        //         'class' => 'form-control',
        //         'placeholder' => 'Entrez une categorie',
        //     ],
        //     'choices' =>  [
        //         '{{$titre_options}}' => $titre_options
        //         ]])

        ->add('recherche', SubmitType::class, [
            'attr' => [
                'class' => 'btn btn-primary'
            ]
        ])
        ->getForm();
      

        $produits = $doctrine->getRepository(Produits::class)->findAll();
        $articles = $paginator->paginate(
            $produits, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
             // Nombre de résultats par page
        );
        $articles->setTemplate('my_pagination.html.twig');
        return $this->render('produits/home.html.twig', [
            'controller_name' => 'ProduitsController',
            'produits' => $articles,
            'form' => $form->createView()
        ]);
    }


    #[Route('/produits', name: 'app_produits')]
    public function index( PaginatorInterface $paginator,ManagerRegistry $doctrine,Request $request, EntityManagerInterface $entityManager): Response
    {
        $post_home =  new Produits();
        $user = $this->getUser();
        $post_home -> addUser($user);
        $produits = $doctrine->getRepository(Produits::class)->findAll();
        $articles = $paginator->paginate(
            $produits, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), 10// Numéro de la page en cours, passé dans l'URL, 1 si aucune page
             // Nombre de résultats par page
        );
     
        return $this->render('produits/productall.html.twig', [
            'controller_name' => 'ProduitsController',
            'produits' => $articles
        ]);
    }

    #[Route('/produit/{id}', name: 'app_produit')]
    public function showProduit(int $id, ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        $user = $this->getUser(); 
        $produit = $doctrine->getRepository(Produits::class)->find($id);
          ;
        return $this->render('produits/product.html.twig', [
            'controller_name' => 'Post',
            'data' => $produit,
        ]);
            
     
    }

    #[Route('/produit/{id}/add', name: 'app_produit_panier')]
    public function buyProduit(int $id, ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        
        $user = $this->getUser(); 
        $userId = $user -> getId();
        $produit = $doctrine->getRepository(Produits::class)->find($id);
        $stock = $produit->getStock();
        $produit = $produit->setStock($stock - 1);
        $user = $user -> addProduit($produit);
     
            $entityManager->persist($user);
            $entityManager->flush();
      
        return new RedirectResponse('/panier');
    }

    #[Route('/produit/{id}/del', name: 'app_produit_del')]
    public function delProduit(int $id, ManagerRegistry $doctrine, Request $request, EntityManagerInterface $entityManager): Response
    {
        
        $user = $this->getUser(); 
        $userId = $user -> getId();
        $produit = $doctrine->getRepository(Produits::class)->find($id);
        $stock = $produit->getStock();
        $produit = $produit->setStock($stock + 1);
        $user = $user -> removeProduit($produit);
        
        $entityManager->persist($user);
        $entityManager->flush();
        
        
        return new RedirectResponse('/panier');
    }
   
    #[Route('/panier', name: 'app_panier')]
    public function getPanier(ManagerRegistry $doctrine, Request $request): Response
    {
        $user = $this->getUser(); 
        $produit = $user->getProduits();
        $array = array();
        foreach($produit as $elm)
    {
    $prix = $elm->getPrix();
    array_push($array ,$prix);
    }
    $final_price = 0;
    foreach($array as $elm)
    {
     $final_price += $elm ;
    }

    $TVA = $final_price * 0.2;
    $size = count($produit);

        return $this->render('produits/panier.html.twig', [
            'controller_name' => 'ProduitsController',
            'produit' => $produit,
            'prix' => $final_price,
            'TVA' => $TVA,
            'size' => $size
        ]);
    }



    #[Route('/about', name: 'app_about')]
    public function about(): Response
    {
        return $this->render('produits/about.html.twig', [
            'controller_name' => 'ProduitsController',
        ]);
    }

    /**
     * @Route("/handleSearch", name="handleSearch")
     * @param Request $request
     */
    public function handleSearch(Request $request, ProduitsRepository $repo, TagsRepository $Tags, ManagerRegistry $doctrine)
    {
        // dd($request);
        // dd($request->request->all('form'));
        
        $query = $request->request->all('form')["query"]; 
        if($query) {
            $produits = $repo->findProduitsByName($query);
        }
        if($produits == null){
            $produits = array();
            $produits_search = $Tags->findByTagsName($query);
 
            $id = $produits_search[0]->getProduits();
        
      
            foreach($id as $elm)
            { 
                array_push($produits ,$elm);
            }
        }

        if(isset($produits) == true){
            return $this->render('produits/index.html.twig', [
                'produits' => $produits,
            ]);
        }
        else{
            return $this->render('produits/index.html.twig', [
              

            ]);
        }
      
    }
}
